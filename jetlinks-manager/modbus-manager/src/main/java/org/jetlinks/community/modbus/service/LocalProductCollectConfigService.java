package org.jetlinks.community.modbus.service;

import lombok.extern.slf4j.Slf4j;
import org.hswebframework.web.crud.service.GenericReactiveCrudService;
import org.jetlinks.community.modbus.entity.ProductCollectConfig;
import org.springframework.stereotype.Service;

/**
 * @author Tensai
 */
@Service
@Slf4j
public class LocalProductCollectConfigService extends GenericReactiveCrudService<ProductCollectConfig, String> {


}
